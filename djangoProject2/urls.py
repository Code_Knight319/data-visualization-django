from django.urls import path
from app import views

urlpatterns = [
    path('index/', views.index),
    path('user/list/',views.user_list),
    path('user/add/',views.user_add),
    path('tpl/',views.tpl),
    path('news/',views.news),
    path('something/',views.something),
    #用户登录
    path('login/',views.login),
    #orm测试
    path('ormtest/',views.ormtest),
    #用户管理
    path('info/list/',views.info_list),
    #添加用户
    path('info/list/add/',views.info_list_add),
    #删除用户
    path('info/list/delete/',views.info_list_delete),
]
