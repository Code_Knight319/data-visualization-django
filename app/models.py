from django.db import models

# Create your models here.
class UserInfo2(models.Model):
    name = models.CharField(max_length=32)
    password = models.CharField(max_length=64)
    age = models.IntegerField(default=18)

class Department(models.Model):
    title = models.CharField(max_length=50)
    Dep_No = models.IntegerField(default=10)
    Dep_Na = models.CharField(max_length=100)
    Dep_data = models.IntegerField(null=True,blank=True)

class Employees(models.Model):
    emp_id = models.CharField(max_length=10)
    position = models.CharField(max_length=50)

class Student(models.Model):
    name = models.CharField(max_length=100)
    sex = models.CharField(max_length=5)
    age = models.IntegerField(default=18)


