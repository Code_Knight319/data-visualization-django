from django.shortcuts import render,HttpResponse,redirect

from app.models import Student,Employees,Department,UserInfo2
# Create your views here.
def index(request):
    return HttpResponse("Hello Django!")

def user_list(requets):
    return render(requets,'user_list.html')

def user_add(requets):
    return render(requets,'user_add.html')

def tpl(requets):
    name="代码骑士"
    name_list=["小明","小红","李华","康康"]
    role_dicts={"name":"小明","salary":100000,"position":"CEO"}
    data_list=[
        {"name": "小明", "salary": 100000, "position": "CEO"},
        {"name": "小红", "salary": 100000, "position": "HR"},
        {"name": "康康", "salary": 100000, "position": "CTO"}
    ]
    return render(requets,'tpl.html',{"n1":name,"n2":name_list,"n3":role_dicts,"n4":data_list})
def news(requet):
    #定义一个列表或字典存储数据
    #向网址：http://www.chinaunicom.com.cn/api/article/NewsByIndex/2/2022/09/news发送请求
    #使用第三方模块：requests
    import requests
    url = "http://www.chinaunicom.com.cn/api/article/NewsByIndex/2/2022/09/news"
    headers = {'User-Agent': 'Mozilla/4.0'}
    res = requests.get(url,headers=headers)
    data_list=res.json()
    print(data_list)
    return render(requet,'news.html',{"news_list":data_list})

def something(request):
    #request是一个对象，封装了用户发送来的所有请求相关的数据
    #1、获取请求方式
    print(request.method)
    #2、在url上面传递值http://127.0.0.1:8000/something/?n1=123&n2=789
    print(request.GET)
    #3、在请求体中提交数据
    print(request.POST)

    #4、【响应】HttpResponse("返回内容"),将字符串返回给请求者
    #return HttpResponse(123)

    #5、【响应】读取HTML的内容+渲染（替换）->字符串（网页源码），返回给用户浏览器
    #return render(request,'something.html',{"title":"requst"})

    #6、【响应】让浏览器重定向
    return redirect("https://www.baidu.com/")

def login(request):
    if request.method == "GET":
        return render(request,'login.html')
    #如果是POST请求，获取用户提交的数据
    print(request.POST)
    username=request.POST.get("user")
    password = request.POST.get("pwd")
    if username == 'root' and password == '123456':
        #return HttpResponse("登录成功！")
        return redirect("https://blog.csdn.net/qq_51701007?spm=1000.2115.3001.5343")
    #return HttpResponse("登录失败！")
    return render(request,'login.html',{"error_msg":"用户名或密码错误！"})

def ormtest(request):
#-------------------1、新建---------------------------
    #student
    #Student.objects.create(name="张三",age=19)
    #Student.objects.create(name="李华",sex="男",age=17)
    #Student.objects.create(name="王刚",sex="女")
    #employees
    #Employees.objects.create(emp_id=10010,position="技术部")
    #Employees.objects.create(emp_id=10012, position="董事部")
    #UserInfo2
    #UserInfo2.objects.create(name="小强",password=123456)
    #UserInfo2.objects.create(name="小明", password=123456)
    #UserInfo2.objects.create(name="小红", password=123456)
#---------------------2、删除------------------------------
    #Student.objects.filter(id=1).delete()
    #Student.objects.all().delete()
#---------------------3、获取------------------------------
    #obj = Employees.objects.filter(emp_id=10010).first()
    #obk = Department.objects.all().first()
    #print(obj)
    #print(obk)
#---------------------4、修改------------------------------
    # UserInfo2.objects.all().update(password="111111")
    # UserInfo2.objects.filter(id=2).update(password="222222")
    # UserInfo2.objects.filter(name="小明").update(password="333333")

    return render(request,'ormtest.html')

def info_list(request):
    #1、获取用户数据库中所有信息
    data_list=UserInfo2.objects.all()
    #2、添加用户
    #、渲染，返回给用户
    return render(request,'info_list.html',{"data_list":data_list})

def info_list_add(request):
    if request.method == "GET":
        return render(request,'info_list_add.html')
    #不执行if语句则请求方法为POST;获取用户提交的数据
    user = request.POST.get("user")
    pwd = request.POST.get("pwd")
    age = request.POST.get("age")
    #添加数据到数据库
    UserInfo2.objects.create(name=user,password=pwd,age=age)
    #自动跳转到用户列表页面
    #return redirect("http://127.0.0.1:8000/info/list/")
    return redirect("/info/list/")

def info_list_delete(request):
    id = request.GET.get('id')
    UserInfo2.objects.filter(id=id).delete()
    return redirect("/info/list/")