# Generated by Django 3.2.15 on 2022-09-11 09:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0016_auto_20220911_1605'),
    ]

    operations = [
        migrations.AddField(
            model_name='userinfo2',
            name='age',
            field=models.IntegerField(default=18),
        ),
    ]
